/* eslint-disable camelcase */
const loki = require('lokijs');
const GeoPoint = require('geopoint');
const dataFile = require('../db/matches.json');

let matchesCollection = {};

function initDB() {
  const db = new loki('matches-db.json');
  matchesCollection = db.addCollection('matches');

  dataFile.matches.forEach((match) => {
    matchesCollection.insert(match);
  });

  db.saveDatabase();
}

function mapReqToQuery(query) {
  const type = {
    'contacts_exchanged:no': () => ({ $eq: 0 }),
    'contacts_exchanged:yes': () => ({ $gt: 0 }),
    'main_photo:no': () => ({ $type: 'undefined' }),
    'main_photo:yes': () => ({ $type: 'string' }),
    'favourite:no': () => ({ $eq: false }),
    'favourite:yes': () => ({ $eq: true }),
    default: () => ({}),
  };

  return (type[query] || type.default)();
}

function filterMatches(req) {
  const query = {};

  const {
    main_photo,
    contacts_exchanged,
    favourite,
    compatibility_score_min,
    compatibility_score_max,
    age_min,
    age_max,
    height_min,
    height_max,
    lon,
    lat,
    range
  } = req.query;

  if (main_photo) {
    query.main_photo = mapReqToQuery(`main_photo:${main_photo}`);
  }
  if (contacts_exchanged) {
    query.contacts_exchanged = mapReqToQuery(`contacts_exchanged:${contacts_exchanged}`);
  }
  if (favourite) {
    query.favourite = mapReqToQuery(`favourite:${favourite}`);
  }

  if (compatibility_score_min && compatibility_score_max) {
    query.compatibility_score = {
      $gte: parseFloat(compatibility_score_min),
      $lte: parseFloat(compatibility_score_max)
    };
  }

  if (age_min && age_max) {
    query.age = { $gte: parseInt(age_min, 10), $lte: parseInt(age_max, 10) };
  }

  if (height_min && height_max) {
    query.height_in_cm = { $gte: parseFloat(height_min), $lte: parseFloat(height_max) };
  }

  let data = matchesCollection.find(query);
  // Uses Geo Bounding Box Query
  if (lon && lat && range) {
    const center = new GeoPoint(parseFloat(lat), parseFloat(lon));
    const coords = center.boundingCoordinates(parseInt(range), 3959, true);
    const NordWestPoint = { lat: coords[0]._degLat, lon: coords[0]._degLon };
    const SudEastPoint = { lat: coords[1]._degLat, lon: coords[1]._degLon };
    data = data.filter((o) => {
      if (
        o.city.lat <= SudEastPoint.lat
        && o.city.lat >= NordWestPoint.lat
        && o.city.lon >= NordWestPoint.lon
        && o.city.lon <= SudEastPoint.lon

      ) {
        return o;
      }
    });
  }
  console.log(data.length);
  return data;
}

module.exports = {
  filterMatches,
  initDB
};
