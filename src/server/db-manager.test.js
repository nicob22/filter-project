const { chai, assert } = require('chai');
const dbManager = require('./db-manager');

describe('dbManager TEST', () => {
  it('it returns Matches without main_photo', (done) => {
    const matches = dbManager.filterMatches({ query: { main_photo: 'no' } });
    assert.equal(3, matches.length);
    done();
  });
});
