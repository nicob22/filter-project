const express = require('express');
const db = require('./db-manager');

const app = express();

db.initDB();

app.use(express.static('dist'));
app.get('/api/matches', (req, res) => {
  const matches = db.filterMatches(req);
  return res.send({ matches });
});

app.listen(8080, () => console.log('Listening on port 8080!'));

module.exports = app;
