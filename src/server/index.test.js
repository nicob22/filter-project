const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('./index');

const should = chai.should();

chai.use(chaiHttp);

describe('/GET Matches', () => {
  it('it should GET all the Matches', (done) => {
    chai.request(server)
      .get('/api/matches')
      .end((err, res) => {
        res.should.have.status(200);
        res.body.matches.should.be.a('array');
        res.body.matches.length.should.be.eql(25);
        done();
      });
  });
});
