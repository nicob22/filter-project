import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Switch from 'react-switch';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';

const Div = styled.div`
    background-color: #f8f9fa!important;
    overflow: hidden;
    color: #0d324d;
    border: 1px solid #e9e9e9;
    -webkit-box-shadow: 0 5px 5px 0 hsla(0,0%,90%,.5);
    box-shadow: 0 5px 5px 0 hsla(0,0%,90%,.5);
    border-radius: 8px;
    position: relative;
    padding:50px;
    margin-bottom: 50px;
`;

const SwitchStyled = styled(Switch)`
  vertical-align: middle;
  margin-left: 4px;
  margin-right: 20px;
`;
const InputRangeStyled = styled(InputRange)`
  padding: 20px!important;
`;

class Controls extends Component {
  state = {
    hasPhoto: false,
    inContacts: false,
    favourite: false,
    score: { min: 1, max: 99 },
    age: { min: 18, max: 95 },
    height: { min: 135, max: 210 },
    distance: 30,
    // User Position for calculating the distance
    userPosition: { lat: 53.701277, lon: -1.648567 }
  };


    updateList = () => {
      this.props.inputChange(this.state);
    }

    handleChange = (field, value) => {
      this.setState({
        [field]: value
      }, this.updateList);
    };

    render() {
      return (
        <Div className="container">
          <label htmlFor="has-photo">
            <span style={{ marginBottom: '3px' }}>Has photo</span>
            <SwitchStyled
              id="has-photo"
              onChange={() => this.handleChange('hasPhoto', !this.state.hasPhoto)}
              checked={this.state.hasPhoto}
            />
          </label>
          <label htmlFor="normal-switch">
            <span style={{ marginBottom: '3px' }}>In Contact</span>
            <SwitchStyled
              id="in-contacts"
              name="inContacts"
              onChange={() => this.handleChange('inContacts', !this.state.inContacts)}
              checked={this.state.inContacts}
            />
          </label>
          <label htmlFor="favourite">
            <span style={{ marginBottom: '3px' }}>Favourite</span>
            <SwitchStyled
              id="favourite"
              name="favourite"
              onChange={() => this.handleChange('favourite', !this.state.favourite)}
              checked={this.state.favourite}
            />
          </label>
          <h4 className="mt-4">Compatibility Score ❤</h4>
          <InputRangeStyled
            maxValue={99}
            minValue={1}
            value={this.state.score}
            onChange={value => this.setState({ score: value }, this.updateList)}
          />
          <h4 className="mt-4">Age 🎂</h4>
          <InputRangeStyled
            maxValue={95}
            minValue={18}
            value={this.state.age}
            onChange={value => this.setState({ age: value }, this.updateList)}
          />
          <h4 className="mt-4">Height ↕</h4>
          <InputRangeStyled
            maxValue={210}
            minValue={135}
            value={this.state.height}
            onChange={value => this.setState({ height: value }, this.updateList)}
          />
          <h4 className="mt-4">Distance in km 🗺</h4>
          <InputRangeStyled
            maxValue={300}
            minValue={30}
            value={this.state.distance}
            onChange={value => this.setState({ distance: value }, this.updateList)}
          />
        </Div>
      );
    }
}

Controls.propTypes = {
  inputChange: PropTypes.func.isRequired
};
export default Controls;
