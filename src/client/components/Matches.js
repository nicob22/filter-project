import React from 'react';
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Col
} from 'reactstrap';
import PropTypes from 'prop-types';
import Loader from 'react-loader';

const Matches = (props) => {
  const { matches, loadingData } = props;
  return (
    <div>
      <div className="card-columns">
        {
          loadingData
            ? <Loader />
            : matches.map(child => (
              <Col key={`${child.height_in_cm + child.display_name}`}>
                <Card>
                  <CardImg
                    top
                    width="100%"
                    src={child.main_photo
                      ? child.main_photo
                      : 'https://images.immediate.co.uk/volatile/sites/3/2017/11/imagenotavailable1-39de324.png?quality=90&resize=620,413'
               }
                    alt="Card image cap"
                  />
                  <CardBody>
                    <CardTitle>
                      {child.display_name}
                      {child.favourite ? '🌟' : ''}
                    </CardTitle>
                    <CardSubtitle>
↕
                      {' '}
                      {child.height_in_cm}
                      {' '}
🎂
                      {' '}
                      {child.age}
                    </CardSubtitle>
                    <CardText>
❤
                      {`${child.compatibility_score * 100}%`}
                      {' '}
                      <br />
🗺
                      {' '}
                      {child.city.name}
                    </CardText>


                  </CardBody>
                </Card>
              </Col>
            ))
       }
      </div>
    </div>
  );
};

Matches.defaultProps = {
  matches: []
};

Matches.propTypes = {
  matches: PropTypes.instanceOf(Array),
  loadingData: PropTypes.bool
};
export default Matches;
