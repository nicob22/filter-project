import React, { Component } from 'react';
import axios from 'axios';
import './App.css';
import {
  Container, Row, Col
} from 'reactstrap';
import Controls from './components/Controls';
import Matches from './components/Matches';

class App extends Component {
  state = {
    loadingData: false,
    matches: [],
  };

  componentDidMount() {
    this.updateMatches('/api/matches');
  }

    getQueryUrl = params => `/api/matches?main_photo=${params.hasPhoto ? 'yes' : 'no'}&contacts_exchanged=${params.inContacts ? 'yes' : 'no'}&favourite=${params.favourite ? 'yes' : 'no'}&compatibility_score_min=${params.score.min / 100}&compatibility_score_max=${params.score.max / 100}&age_min=${params.age.min}&age_max=${params.age.max}&height_min=${params.height.min}&height_max=${params.height.max}&lon=${params.userPosition.lon}&lat=${params.userPosition.lat}&range=${params.distance}`;

    updateMatches = (url) => {
      this.setState({ loadingData: true }, () => {
        axios.get(url)
          .then((res) => {
            this.setState({
              matches: res.data.matches,
              loadingData: false
            });
          })
          .catch(err => console.log(err));
      });
    };

    handleSubmit = (params) => {
      this.updateMatches(this.getQueryUrl(params));
    };


    render() {
      return (
        <div>
          <nav className="navbar navbar-light bg-light">
            <span className="navbar-brand mb-0 h1">Matches</span>
          </nav>
          <Container>
            <Row>
              <Col>
                <Controls inputChange={this.handleSubmit} />
                <Matches matches={this.state.matches} loadingData={this.state.loadingData} />
              </Col>
            </Row>
          </Container>
        </div>
      );
    }
}

export default App;
