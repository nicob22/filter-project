# filter-project

[![Build Status](https://travis-ci.org/crsandeep/simple-react-full-stack.svg?branch=master)](https://travis-ci.org/crsandeep/simple-react-full-stack)

A web application wich filter and shows the user matches
This project is built using a [simple-react-full-stack](https://github.com/crsandeep/simple-react-full-stack) and uses React, Node.js, Express and Webpack.

- [filter-project](#filter-project)
  - [Requirements](#requirements)
  - [Quick Start](#quick-start)
    - [Development mode](#development-mode)
    - [Production mode](#production-mode)
    - [Database](#database)
  - [Tests](#tests)


## Requirements

- Node.js `8.9.4`
- npm `5.6.0`


## Quick Start

```bash
# Clone the repository
git clone
https://bitbucket.org/nicob22/filter-project

# Go inside the directory
cd filter-project

# Install dependencies
npm install

# Start development server
npm run dev

# Build for production
npm run build

# Start production server
npm start
```

### Development mode

In the development mode, we will have 2 servers running. The front end code will be served by the [webpack dev server](https://webpack.js.org/configuration/dev-server/) which helps with hot and live reloading. The server side Express code will be served by a node server using [nodemon](https://nodemon.io/) which helps in automatically restarting the server whenever server side code changes.

### Production mode

In the production mode, we will have only 1 server running. All the client side code will be bundled into static files using webpack and it will be served by the Node.js/Express application.

### Database

This application uses an in-memory document-oriented datastore for node.js
To learn more [lokijs](http://lokijs.org/)

## Tests
```bash

# Runs the tests
npm run test

# Runs test and shows the coverage
npm run cover

```
